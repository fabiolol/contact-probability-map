# Contact Probability Map

Here you can find my Jupiter-notebook containing the gromacs commands to calculate the contacts and the python code to plot the map.

If there is an issue with the code, please let me know.

![Contact Probability](example.png)


